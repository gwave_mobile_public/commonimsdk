//
//  ViewController.swift
//  CommonIMSDK
//
//  Created by yangpeng on 06/14/2023.
//  Copyright (c) 2023 yangpeng. All rights reserved.
//

import UIKit
import CommonIMSDK

class ViewController: UIViewController {

    public let imAppID = 20001744
    public let imSecretKey = "c2f8b2e95174095d18960c20fbc5dbeaf5b5fbca8c5d47655592204314073a6c"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //初始化IM
        
        let loginButton = UIButton(frame: CGRectMake(20, 100, 80, 40))
        loginButton.setTitle("login", for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        loginButton.setTitleColor(UIColor.white, for: .normal)
        loginButton.backgroundColor = UIColor.purple
        loginButton.addTarget(self, action:#selector(loginClick), for: .touchUpInside)
        self.view.addSubview(loginButton)
        
        let logoutButton = UIButton(frame: CGRectMake(120, 100, 80, 40))
        logoutButton.setTitle("logout", for: .normal)
        logoutButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        logoutButton.setTitleColor(UIColor.white, for: .normal)
        logoutButton.backgroundColor = UIColor.purple
        logoutButton.addTarget(self, action:#selector(logoutClick), for: .touchUpInside)
        self.view.addSubview(logoutButton)
        
    }

    @objc func loginClick() {
        IMBridge.sharedInstance().initIMSDK(Int32(imAppID))
        let userId = "2021092411014373676516"
        let userSig = "eAEtzE0LgkAUheH-ctch947TXEdoYSBESFBGhDvT0YZIRUX6oP*eNG0fznnfcExSbyhvedfZEkKBiMRSLn4*2ruBkFTAmphZOx1sDSFc5DofdZxsd9cD1fvntHmlcayzosFKt36NQ5ud*JxGfbECdzSPzvZzUKEMEJ1Npp9jwsP-xpamGW1lHaMg1EISIUmffcVqSQo*X7hhMuY_"
        print("userSig = \(userSig)")
        IMBridge.sharedInstance().loginIMSDK(userId, userSig: userSig) {
            print("IM Login Success")
            IMBridge.sharedInstance().setAPNS("15565");
        } fail: { code, message in
            print("IM Login Fail \(code),\(message)")
        }
        
//        let userId1 = "2021110510222717984500"
//        let userSig1 = IMBridge.genTestUserSig(userId1, imAppId: Int32(imAppID), imSecretKey: imSecretKey)
//        print("userSig = \(userSig1)")
//        IMBridge.sharedInstance().loginIMSDK(userId1, userSig: userSig1) {
//            print("IM Login Success")
//            IMBridge.sharedInstance().setAPNS("15565");
//        } fail: { code, message in
//            print("IM Login Fail \(code),\(message)")
//        }
        
    }
    
    @objc func logoutClick(){
        print("-----",IMBridge.sharedInstance().getLoginUser())

//        IMBridge.sharedInstance().logout {
//            print("IM Logout Success")
//        } fail: { code, message in
//            print("IM Logout Fail \(code),\(message)")
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

