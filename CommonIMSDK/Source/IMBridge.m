//
//  IMBridge.m
//  Pods-CommonIMSDK_Example
//
//  Created by 杨鹏 on 2023/6/14.
//

#import "IMBridge.h"
#import <zlib.h>
#import <CommonCrypto/CommonCrypto.h>

@interface IMBridge()<V2TIMConversationListener,V2TIMAdvancedMsgListener,V2TIMAPNSListener>

@property (nonatomic, strong) NSData *pushToken;

@end

@implementation IMBridge

+ (instancetype)sharedInstance
{
  static IMBridge *shareInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    shareInstance = [[[self class] alloc] init];
  });
  return shareInstance;
}

//初始化SDK
- (void)initIMSDK:(int)imAppId
{
    //初始化
    V2TIMSDKConfig *config = [[V2TIMSDKConfig alloc] init];
    config.logLevel = V2TIM_LOG_INFO;
    config.logListener = ^(V2TIMLogLevel logLevel, NSString *logContent) {
        
    };
    [[V2TIMManager sharedInstance] initSDK:imAppId config:config];
}

- (void)unInitIMSDK
{
    [[V2TIMManager sharedInstance] unInitSDK];
}

- (void)imPushToken:(NSData *)pushToken
{
  self.pushToken = pushToken;
}

- (void)setAPNS:(NSString *)businessID
{
    if (self.pushToken) {
        [V2TIMManager.sharedInstance setAPNSListener:self];
        V2TIMAPNSConfig *confg = [[V2TIMAPNSConfig alloc] init];
       // 企业证书 ID
       // 用户自己到苹果注册开发者证书，在开发者帐号中下载并生成证书(p12 文件)，将生成的 p12 文件传到腾讯证书管理控制台，
       // 控制台会自动生成一个证书 ID，将证书 ID 传入一下 busiId 参数中。
       confg.businessID = businessID.intValue;
       confg.token = self.pushToken;
       [[V2TIMManager sharedInstance] setAPNS:confg succ:^{
            NSLog(@"%s, succ", __func__);
       } fail:^(int code, NSString *msg) {
            NSLog(@"%s, fail, %d, %@", __func__, code, msg);
       }];
    }
}

//登录
- (void)loginIMSDK:(NSString *)userID userSig:(NSString *)userSig succ:(CommonSucc)succ fail:(CommonFail)fail
{
    if (userID.length > 0 && userSig.length > 0) {
        [[V2TIMManager sharedInstance] login:userID userSig:userSig succ:succ fail:fail];
    } else {
        fail(0,@"userID或者userSig为空");
    }
}

- (void)logout:(CommonSucc)succ fail:(CommonFail)fail
{
    [[V2TIMManager sharedInstance] logout:succ fail:fail];
}

- (NSString *)getLoginUser
{
    return [[V2TIMManager sharedInstance] getLoginUser];
}

- (uint64_t)getServerTime
{
    return [[V2TIMManager sharedInstance] getServerTime];
}

//群组会话监听
- (void)addConversationListener
{
    [[V2TIMManager sharedInstance] addConversationListener:self];
}

//群组会话移除监听
- (void)removeConversationListener
{
    [[V2TIMManager sharedInstance] removeConversationListener:self];
}

//添加消息监听器
- (void)addAdvancedMsgListener
{
    [[V2TIMManager sharedInstance] addAdvancedMsgListener:self];
}

//移除消息监听器
- (void)removeAdvancedMsgListener
{
    [[V2TIMManager sharedInstance] removeAdvancedMsgListener:self];
}

- (void)setSelfInfo:(NSDictionary *)Info succ:(V2TIMSucc)succ fail:(V2TIMFail)fail
{
    V2TIMUserFullInfo *userInfo = [[V2TIMUserFullInfo alloc] init];
    userInfo.faceURL = [Info objectForKey:@"faceURL"];
    userInfo.nickName = [Info objectForKey:@"nickName"];
    [[V2TIMManager sharedInstance] setSelfInfo:userInfo succ:succ fail:fail];
}

- (void)sendGroupTextMessage:(NSString *)text to:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail
{
    if (text && groupID) {
        [[V2TIMManager sharedInstance] sendGroupTextMessage:text to:groupID priority:V2TIM_PRIORITY_NORMAL succ:succ fail:fail];
    }
}

- (void)sendGroupCustomMessage:(NSData *)customData to:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail
{
    if (customData && groupID) {
        [[V2TIMManager sharedInstance] sendGroupCustomMessage:customData to:groupID priority:V2TIM_PRIORITY_HIGH succ:succ fail:fail];
    }
}

//指定人发送文本消息
- (void)sendC2CTextMessage:(NSString *)text to:(NSString *)userID succ:(CommonSucc)succ fail:(CommonFail)fail;
{
    [[V2TIMManager sharedInstance] sendC2CTextMessage:text to:userID succ:succ fail:fail];
}

//获取群成员列表
- (void)getGroupMemberList:(NSString*)groupID succ:(V2TIMGroupMemberInfoResultSucc)succ fail:(V2TIMFail)fail
{
    [[V2TIMManager sharedInstance] getGroupMemberList:groupID filter:V2TIM_GROUP_MEMBER_FILTER_ALL nextSeq:0 succ:succ fail:fail];
}

- (void)createGroup:(NSString *)groupName succ:(V2TIMCreateGroupSucc)succ fail:(V2TIMFail)fail
{
    //groupType:群类型
    [[V2TIMManager sharedInstance] createGroup:GroupType_Work groupID:nil groupName:groupName succ:succ fail:fail];
}

- (void)joinGroup:(NSString*)groupID msg:(NSString*)msg succ:(CommonSucc)succ fail:(CommonFail)fail
{
    [[V2TIMManager sharedInstance] joinGroup:groupID msg:msg succ:succ fail:fail];
}

- (void)quitGroup:(NSString*)groupID succ:(CommonSucc)succ fail:(CommonFail)fail
{
    [[V2TIMManager sharedInstance] quitGroup:groupID succ:succ fail:fail];
}

- (void)inviteUserToGroup:(NSString*)groupID userList:(NSArray<NSString *>*)userList succ:(CommonSucc)succ fail:(CommonFail)fail
{
    [[V2TIMManager sharedInstance] inviteUserToGroup:groupID userList:userList succ:^(NSArray<V2TIMGroupMemberOperationResult *> *resultList) {
        succ();
    } fail:^(int code, NSString *desc) {
        fail(code,desc);
    }];
}

- (void)getConversationUnreadMessageCount:(NSString *)conversationID unreadCount:(KGroupUnreadCount)unreadCount;
{
    [[V2TIMManager sharedInstance] getConversation:[NSString stringWithFormat:@"group_%@",conversationID]  succ:^(V2TIMConversation *conv) {
        unreadCount(conv.unreadCount);
    } fail:^(int code, NSString *desc) {
        unreadCount(0);
    }];
}

- (void)getTotalUnreadMessageCount:(KGroupUnreadCount)unreadCount;
{
    [[V2TIMManager sharedInstance] getTotalUnreadMessageCount:^(UInt64 totalCount) {
        unreadCount(totalCount);
    } fail:^(int code, NSString *desc) {
        unreadCount(0);
    }];
}

//清除全部未读消息
/**
 *  - 当您想清理所有单聊会话的未读消息计数，conversationID 请传入 @"c2c"，即不指定具体的 userID；
 *  - 当您想清理所有群聊会话的未读消息计数，conversationID 请传入 @"group"，即不指定具体的 groupID；
 *  - 当您想清理所有会话的未读消息计数，conversationID 请传入 @“”  或者 nil；
 *  - 该接口调用成功后，SDK 会通过 onConversationChanged 回调将对应会话的最新未读数通知给您。
 */
 
- (void)markC2CMessageAsRead:(NSString *)conversationID succ:(V2TIMSucc)succ fail:(V2TIMFail)fail
{
    [[V2TIMManager sharedInstance] cleanConversationUnreadMessageCount:conversationID cleanTimestamp:0 cleanSequence:0 succ:succ fail:fail];
}

//全员禁言
- (void)setGroupInfo:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail
{
    V2TIMGroupInfo *info = [[V2TIMGroupInfo alloc] init];
    info.groupID = groupID;
    info.allMuted = YES;
    [[V2TIMManager sharedInstance] setGroupInfo:info succ:succ fail:fail];
}

//获取群聊天列表，内部封装带筛选的高级接口
- (void)getConversationList:(uint64_t)nextSeq count:(uint32_t)count succ:(V2TIMConversationResultSucc)succ fail:(V2TIMFail)fail
{
    V2TIMConversationListFilter *listFilter = [[V2TIMConversationListFilter alloc] init];
    listFilter.type = V2TIM_C2C;
    [[V2TIMManager sharedInstance] getConversationListByFilter:listFilter nextSeq:nextSeq count:count succ:succ fail:fail];
}

//获取历史聊天消息
- (void)getC2CHistoryMessageList:(NSString *)userID count:(int)count lastMsg:(V2TIMMessage*)lastMsg succ:(V2TIMMessageListSucc)succ fail:(V2TIMFail)fail
{
    [[V2TIMManager sharedInstance] getC2CHistoryMessageList:userID count:count lastMsg:lastMsg succ:succ fail:fail];
}

#pragma mark V2TIMConversationListener
//未读消息数及最后一条消息更新
- (void)onConversationChanged:(NSArray<V2TIMConversation*> *)conversationList
{
    [conversationList enumerateObjectsUsingBlock:^(V2TIMConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"onConversationChanged" object:nil userInfo:@{@"conversation":obj}];
    }];
}

//有新的会话（比如收到一个新同事发来的单聊消息、或者被拉入了一个新的群组中），可以根据会话的 lastMessage -> timestamp 重新对会话列表做排序。
- (void)onNewConversation:(NSArray<V2TIMConversation*> *)conversationList
{
    [conversationList enumerateObjectsUsingBlock:^(V2TIMConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"onConversationChanged" object:nil userInfo:@{@"conversation":obj}];
    }];
}

#pragma mark -V2TIMAPNSListener

- (uint32_t)onSetAPPUnreadCount
{
    return 0;
}

#pragma mark - V2TIMAdvancedMsgListener

- (void)onRecvNewMessage:(V2TIMMessage *)msg
{
    //高级接口，所有类型消息均会调用,msg.groupID,msg.userID 可以区分当前消息为群聊，单聊
    if (msg.userID.length > 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"onRecvNewMessage" object:nil userInfo:@{@"recvNewMessage":msg}];
    }
}

//清除指定群组未读消息数，群已经解散
- (void)markGroupMessageAsRead:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail {
    [[V2TIMManager sharedInstance] cleanConversationUnreadMessageCount:groupID cleanTimestamp:0 cleanSequence:0 succ:succ fail:fail];
}

#warning test
+ (NSString *)genTestUserSig:(NSString *)identifier imAppId:(int)imAppId imSecretKey:(NSString *)imSecretKey
{
    CFTimeInterval current = CFAbsoluteTimeGetCurrent() + kCFAbsoluteTimeIntervalSince1970;
    long TLSTime = floor(current);
    NSMutableDictionary *obj = [@{@"TLS.ver": @"2.0",
                                  @"TLS.identifier": identifier,
                                  @"TLS.sdkappid": @(imAppId),
                                  @"TLS.expire": @(604800),
                                  @"TLS.time": @(TLSTime)} mutableCopy];
    NSMutableString *stringToSign = [[NSMutableString alloc] init];
    NSArray *keyOrder = @[@"TLS.identifier",
                          @"TLS.sdkappid",
                          @"TLS.time",
                          @"TLS.expire"];
    for (NSString *key in keyOrder) {
        [stringToSign appendFormat:@"%@:%@\n", key, obj[key]];
    }
    NSLog(@"%@", stringToSign);
    //NSString *sig = [self sigString:stringToSign];
    NSString *sig = [self hmac:stringToSign imSecretKey:imSecretKey];

    obj[@"TLS.sig"] = sig;
    NSLog(@"sig: %@", sig);
    NSError *error = nil;
    NSData *jsonToZipData = [NSJSONSerialization dataWithJSONObject:obj options:0 error:&error];
    if (error) {
        NSLog(@"[Error] json serialization failed: %@", error);
        return @"";
    }

    const Bytef* zipsrc = (const Bytef*)[jsonToZipData bytes];
    uLongf srcLen = jsonToZipData.length;
    uLong upperBound = compressBound(srcLen);
    Bytef *dest = (Bytef*)malloc(upperBound);
    uLongf destLen = upperBound;
    int ret = compress2(dest, &destLen, (const Bytef*)zipsrc, srcLen, Z_BEST_SPEED);
    if (ret != Z_OK) {
        NSLog(@"[Error] Compress Error %d, upper bound: %lu", ret, upperBound);
        free(dest);
        return @"";
    }
    NSString *result = [self base64URL: [NSData dataWithBytesNoCopy:dest length:destLen]];
    return result;
}

+ (NSString *)hmac:(NSString *)plainText imSecretKey:(NSString *)imSecretKey
{
    const char *cKey  = [imSecretKey cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [plainText cStringUsingEncoding:NSASCIIStringEncoding];

    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];

    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);

    NSData *HMACData = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    return [HMACData base64EncodedStringWithOptions:0];
}

+ (NSString *)base64URL:(NSData *)data
{
    NSString *result = [data base64EncodedStringWithOptions:0];
    NSMutableString *final = [[NSMutableString alloc] init];
    const char *cString = [result cStringUsingEncoding:NSUTF8StringEncoding];
    for (int i = 0; i < result.length; ++ i) {
        char x = cString[i];
        switch(x){
            case '+':
                [final appendString:@"*"];
                break;
            case '/':
                [final appendString:@"-"];
                break;
            case '=':
                [final appendString:@"_"];
                break;
            default:
                [final appendFormat:@"%c", x];
                break;
        }
    }
    return final;
}



@end
