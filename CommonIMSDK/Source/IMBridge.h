//
//  IMBridge.h
//  Pods-CommonIMSDK_Example
//
//  Created by 杨鹏 on 2023/6/14.
//

#import <Foundation/Foundation.h>
#import <ImSDK_Plus/ImSDK_Plus.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CommonFail)(int code, NSString * msg);
typedef void (^CommonSucc)(void);
typedef void (^KGroupCreateSuc)(NSString * _Nullable groupId);
typedef void (^KGroupUnreadCount)(NSInteger unreadCount);

@interface IMBridge : NSObject

+ (instancetype)sharedInstance;

//初始化SDK
- (void)initIMSDK:(int)imAppId;

//反初始化SDK
- (void)unInitIMSDK;

//推送Token设置
- (void)imPushToken:(NSData *)pushToken;

//设置推送Id
- (void)setAPNS:(NSString *)businessID;

//登录
- (void)loginIMSDK:(NSString *)userID userSig:(NSString *)userSig succ:(CommonSucc)succ fail:(CommonFail)fail;

//登出
- (void)logout:(CommonSucc)succ fail:(CommonFail)fail;

//获取用户资料
- (NSString *)getLoginUser;

//获取IM登录时间
- (uint64_t)getServerTime;

//群组会话监听
- (void)addConversationListener;

//群组会话移除监听
- (void)removeConversationListener;

//添加消息监听器
- (void)addAdvancedMsgListener;

//移除消息监听器
- (void)removeAdvancedMsgListener;

//设置用户信息
- (void)setSelfInfo:(NSDictionary *)Info succ:(V2TIMSucc)succ fail:(V2TIMFail)fail;

//指定群发送文本消息
- (void)sendGroupTextMessage:(NSString *)text to:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail;

//指定群组发送自定义消息
- (void)sendGroupCustomMessage:(NSData *)customData to:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail;

//指定人发送文本消息,返回消息的唯一标识
- (void)sendC2CTextMessage:(NSString *)text to:(NSString *)userID succ:(CommonSucc)succ fail:(CommonFail)fail;

//获取群成员列表
- (void)getGroupMemberList:(NSString*)groupID succ:(V2TIMGroupMemberInfoResultSucc)succ fail:(V2TIMFail)fail;

//创建讨论组
- (void)createGroup:(NSString *)groupName succ:(V2TIMCreateGroupSucc)succ fail:(V2TIMFail)fail;

//讨论组添加成员
- (void)joinGroup:(NSString*)groupID msg:(NSString*)msg succ:(CommonSucc)succ fail:(CommonFail)fail;

//邀请讨论组成员
- (void)inviteUserToGroup:(NSString*)groupID userList:(NSArray<NSString *>*)userList succ:(CommonSucc)succ fail:(CommonFail)fail;

//退出讨论组
- (void)quitGroup:(NSString*)groupID succ:(CommonSucc)succ fail:(CommonFail)fail;

//获取指定讨论组未读消息数
- (void)getConversationUnreadMessageCount:(NSString *)conversationID unreadCount:(KGroupUnreadCount)unreadCount;

//获取当前未读消息总数
- (void)getTotalUnreadMessageCount:(KGroupUnreadCount)unreadCount;

//清除指定群组未读消息数，群已经解散
- (void)markGroupMessageAsRead:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail;

//清除全部未读消息
- (void)markC2CMessageAsRead:(NSString *)conversationID succ:(V2TIMSucc)succ fail:(V2TIMFail)fail;

//全员禁言
- (void)setGroupInfo:(NSString *)groupID succ:(CommonSucc)succ fail:(CommonFail)fail;

//获取群聊天列表，内部封装带筛选的高级接口
- (void)getConversationList:(uint64_t)nextSeq count:(uint32_t)count succ:(V2TIMConversationResultSucc)succ fail:(V2TIMFail)fail;

//获取历史聊天消息
- (void)getC2CHistoryMessageList:(NSString *)userID count:(int)count lastMsg:(V2TIMMessage*)lastMsg succ:(V2TIMMessageListSucc)succ fail:(V2TIMFail)fail;

#warning test
+ (NSString *)genTestUserSig:(NSString *)identifier imAppId:(int)imAppId imSecretKey:(NSString *)imSecretKey;


@end

NS_ASSUME_NONNULL_END
