# CommonIMSDK

[![CI Status](https://img.shields.io/travis/yangpeng/CommonIMSDK.svg?style=flat)](https://travis-ci.org/yangpeng/CommonIMSDK)
[![Version](https://img.shields.io/cocoapods/v/CommonIMSDK.svg?style=flat)](https://cocoapods.org/pods/CommonIMSDK)
[![License](https://img.shields.io/cocoapods/l/CommonIMSDK.svg?style=flat)](https://cocoapods.org/pods/CommonIMSDK)
[![Platform](https://img.shields.io/cocoapods/p/CommonIMSDK.svg?style=flat)](https://cocoapods.org/pods/CommonIMSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CommonIMSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CommonIMSDK'
```

## Author

yangpeng, peng.yang@kikitrade.com

## License

CommonIMSDK is available under the MIT license. See the LICENSE file for more info.
